﻿using AOSharp.Clientless;
using AOSharp.Clientless.Chat;
using AOSharp.Clientless.Logging;
using AOSharp.Common.GameData;
using SmokeLounge.AOtomation.Messaging.Messages;
using System.Linq;

namespace TestClientlessPlugin
{
    public class Main : ClientlessPluginEntry
    {
        public override void Init(string pluginDir)
        {
            Logger.Information("TestClientlessPlugin::Init");

            //Client.SuppressDeserializationErrors();

            Client.OnUpdate += OnUpdate;
            Client.MessageReceived += OnMessageReceived;

            Client.Chat.PrivateMessageReceived += (e, msg) => HandlePrivateMessage(msg);
            Client.Chat.PrivateGroupMessageReceived += (e, msg) => HandlePrivateGroupMessage(msg);
            Client.Chat.PrivateGroupInviteMessageReceived += (e, charId) => HandlePrivateGroupInviteMessageReceived(charId);
            Client.Chat.VicinityMessageReceived += (e, msg) => HandleVicinityMessage(msg);
            Client.Chat.GroupMessageReceived += (e, msg) => HandleGroupMessage(msg);

            Team.TeamRequest += OnTeamRequested;
            Team.TeamMember += OnTeamMember;
            Team.TeamMemberLeft += OnTeamMemberLeft;
            Team.TeamRequestResponse += OnTeamRequestResponse;

            DynelManager.DynelSpawned += OnDynelSpawned;
            DynelManager.DynelDespawned += OnDynelDespawned;
            DynelManager.DynelUsed += OnDynelUsed;

            Playfield.TowerUpdate += OnTowerUpdate;

            BuffStatus.BuffChanged += OnBuffChanged;

            Inventory.Bank.Opened += OnBankOpen;
        }

        private void OnBankOpen()
        {
            Logger.Debug($"My bank is open!");
        }

        private void OnTeamRequestResponse(object sender, TeamRequestResponseEventsArgs e)
        {
            Logger.Debug($"OnTeamRequestResponse Event: Identity: {e.Identity}, Response: {e.Response}");
        }

        private void OnTeamMemberLeft(object sender, TeamMemberLeftEventsArgs e)
        {
            Logger.Debug($"TeamMemberLeft Event: Identity: {e.Identity} Current team members ({Team.Members.Count}):");

            foreach (TeamMember member in Team.Members)
                Logger.Debug($"{member.Identity}");
        }

        private void OnTeamMember(object sender, TeamMemberEventsArgs e)
        {
            Logger.Debug($"TeamMember Event: Identity: {e.Identity}, Current team members ({Team.Members.Count}):");

            foreach (TeamMember member in Team.Members)
                Logger.Debug($"{member.Identity}");
        }

        private void OnBuffChanged(object sender, BuffChangedArgs e)
        {
            Logger.Debug($"OnBuffChanged Event: Identity: {e.Identity}, BuffId: {e.Id}, Status: {e.Status}");
        }

        private void OnTeamRequested(object sender, TeamRequestEventArgs e)
        {
            e.Accept();
        }

        private void OnUpdate(object _, double deltaTime)
        {
            //Logger.Debug("OnUpdate");
        }

        private void HandleVicinityMessage(VicinityMsg msg)
        {
            Logger.Debug($"{msg.SenderName}: {msg.Message}");
        }

        private void HandlePrivateGroupMessage(PrivateGroupMsg msg)
        {
            Logger.Debug($"[{msg.ChannelId}|{msg.ChannelName}] {msg.SenderId}|{msg.SenderName}: {msg.Message}");
        }

        private void HandlePrivateGroupInviteMessageReceived(PrivateGroupInviteArgs args)
        {
            Logger.Debug($"Invited to private group from CharId: {args.Requester}");

            if (args.Requester == 1134420643)
            {
                Logger.Debug($"Accepted private group invite from: {args.Requester}");

                PrivateGroupChat.Join(args.Requester);
            }
        }

        private void HandlePrivateMessage(PrivateMessage msg)
        {
            if (!msg.Message.StartsWith("!"))
                return;

            string[] commandParts = msg.Message.Split(' ');

            switch (commandParts[0].Remove(0, 1).ToLower())
            {
                case "testjoin":
                    //PrivateGroupChat.Join(1134420643);
                    break;
                case "test":
                    Logger.Information("Spell List:");

                    foreach (int spellId in DynelManager.LocalPlayer.SpellList)
                        Logger.Information($"\t{spellId}");
                    break;
                case "pf":
                    Logger.Information(Playfield.Name);
                    break;
                case "cast":
                    Logger.Information($"Received cast request from {msg.SenderName}");

                    int nanoId;

                    if (commandParts.Length < 2 || !int.TryParse(commandParts[1], out nanoId))
                    {
                        Logger.Error($"Received cast command without valid params.");
                        return;
                    }

                    if (DynelManager.Find(msg.SenderName, out PlayerChar requestor))
                        DynelManager.LocalPlayer.Cast(requestor, nanoId);
                    else
                        Logger.Error($"Unable to locate requestor.");

                    break;
                case "stand":
                    Logger.Information($"Received stand request from {msg.SenderName}");
                    DynelManager.LocalPlayer.MovementComponent.ChangeMovement(MovementAction.LeaveSit);
                    break;
                case "sit":
                    Logger.Information($"Received sit request from {msg.SenderName}");
                    DynelManager.LocalPlayer.MovementComponent.ChangeMovement(MovementAction.SwitchToSit);
                    break;
                case "isbankopen":
                    Logger.Information(Inventory.Bank.IsOpen.ToString());
                    break;
                case "usebank":
                    {
                        Logger.Information($"Using bank");

                        //bor fair trade bank
                        Dynel bank = new Dynel(new Identity(IdentityType.Terminal, 0x4C5B66EB));
                        bank.Use();
                    }
                    break;
                case "usesitkit":
                    if (Inventory.Items.Find((int)RelevantItems.PremiumHealthAndNanoRecharger, out Item item))
                    {
                        Client.SendPrivateMessage(msg.SenderId, $"| Using | Item: {item.Id}/{item.HighId}/{item.Ql} | Slot: {item.Slot} | Type !checksitkit for a cooldown timer");
                        DynelManager.LocalPlayer.MovementComponent.ChangeMovement(MovementAction.SwitchToSit);
                        item.Use();
                        DynelManager.LocalPlayer.MovementComponent.ChangeMovement(MovementAction.LeaveSit);
                    }
                    break;
                case "checksitkit":
                    if (!DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Treatment))
                    {
                        var message = "Sit kit is ready to be used!";
                        Client.SendPrivateMessage(msg.SenderId, message);
                    }
                    else
                    {
                        DynelManager.LocalPlayer.Cooldowns.Find(Stat.Treatment, out Cooldown cooldown);
                        Client.SendPrivateMessage(msg.SenderId, $"Sitkit on cooldown - Remaining time: {cooldown.RemainingTime} seconds.");
                    }
                    break;
                case "checkbufftimers":
                    var dynel = DynelManager.Characters.FirstOrDefault(x => x.Identity.Instance == msg.SenderId);
                    if (dynel == null)
                    {
                        Client.SendPrivateMessage(msg.SenderId, "Dynel not found (out of range most likely).");
                        return;
                    }
                    break;
                case "testinter":
                    if (Inventory.Find("Health and Nano Stim", out Item stim))
                    {
                        foreach (var useCriterias in stim.Criteria[ItemActionInfo.UseCriteria])
                            Logger.Information($"Param1: {useCriterias.Param1} Param2: {useCriterias.Param2} Operator: {useCriterias.Operator}");

                        Logger.Information($"CanUse: {stim.MeetsUseReqs()}");
                    }
                    break;
                case "orgchat":
                    Client.SendOrgMessage("I've said something in org chat!");
                    break;
                case "teamchat":
                    Client.SendTeamMessage("I've said something in team chat!");
                    break;
                case "otoocchat":
                    Client.SendChannelMessage(ChannelType.OTOOC, "I've said something in ot ooc chat!");
                    break;
                default:
                    Logger.Warning($"Received unknown command: {msg.Message}");
                    break;
            }
        }

        private void HandleGroupMessage(GroupMsg msg)
        {
            if (msg.ChannelId == DynelManager.LocalPlayer.OrgId)
            {
                Logger.Information($"Received org message!");
            }
            else if (msg.ChannelId == DynelManager.LocalPlayer.TeamId)
            {
                Logger.Information($"Received team message!");
            }

            Logger.Information($"[{msg.ChannelName}]: {msg.SenderName}: {msg.Message}");
        }

        private void OnDynelSpawned(object _, Dynel dynel)
        {
            if (dynel is PlayerChar player)
            {
                Logger.Information($"Player Spawned: {player.Name} - {player.Transform.Position} - {player.Side}");
            }
            else if (dynel is NpcChar npc)
            {
                Logger.Information($"NPC Spawned: {npc.Name} {(npc.Owner.HasValue ? $"- Owner: {npc.Owner}" : "")}- {npc.Transform.Position}");
            }
        }

        private void OnDynelDespawned(object _, Dynel dynel)
        {
            if (dynel is SimpleChar simpleChar)
                Logger.Information($"Character Despawned: {simpleChar.Name} - {simpleChar.Transform.Position}");
        }

        private void OnDynelUsed(Identity user, Identity target)
        {
            Logger.Information($"Dynel Used - User: {user} - Target: {target}");
        }

        private void OnMessageReceived(object _, Message msg)
        {
            if (msg.Header.PacketType == PacketType.PingMessage)
                Logger.Debug($"Plugin detected Ping message!");
        }

        private void OnTowerUpdate(object _, TowerUpdateEventArgs e)
        {
            Logger.Debug($"{e.Tower.Side} {e.Tower.Class} {e.Tower.TowerCharId} at {e.Tower.Position} - {e.UpdateType}");
        }
    }
}

public enum RelevantItems
{
    PremiumHealthAndNanoRecharger = 297274
}